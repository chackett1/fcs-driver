package main

import (
	"encoding/base64"
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"time"
)

const (
	fcsURL = "https://127.0.0.1:8443"
)

const (
	chromedriverPath = "chromedriver"
	driverPort       = 8080
	pageLoadTimeout  = time.Duration(time.Second * 25)
)

func main() {

	b, err := NewBot(false,false, false)
	if err != nil {
		logrus.WithError(err).Fatal("NewBot")
	}

	err = b.LaunchFCS()
	if err != nil {
		logrus.WithError(err).Fatal("LaunchFCS")
	}

	err = b.pollPage("/wizard/continue-or-start")
	if err != nil {
		logrus.WithError(err).Fatal("poll start page")
	}

	err = b.doStartPageActions()
	if err != nil {
		logrus.WithError(err).Fatal("do start page actions")
	}

	err = b.pollPage("/wizard/discovery-config")
	if err != nil {
		logrus.WithError(err).Fatal("poll discovery config page")
	}

	err = b.doDiscoveryPageActions()
	if err != nil {
		logrus.WithError(err).Fatal("do discovery actions")
	}

	err = b.pollPage("/wizard/configuration")
	if err != nil {
		logrus.WithError(err).Fatal("poll configuration page")
	}

	err = b.doConfigActions()
	if err != nil {
		logrus.WithError(err).Fatal("do config actions")
	}

	err = b.pollPage("/wizard/overview-run")
	if err != nil {
		logrus.WithError(err).Fatal("poll overview run page")
	}

	err = b.doOverviewRunActions()
	if err != nil {
		logrus.WithError(err).Fatal("do overview run actions")
	}
}

func (b *Bot) typ(msg string) error {
	for _, k := range msg {
		err := b.wd.KeyDown(string(k))
		if err != nil {
			return errors.Wrap(err, "KeyDown v")
		}
		err = b.wd.KeyUp(string(k))
		if err != nil {
			return errors.Wrap(err, "KeyUp meta")
		}
	}
	return nil
}

func (b *Bot) findSelectJSONTextArea() error {
	ta, err := b.wd.FindElements(selenium.ByCSSSelector, ".ace_content")
	if err != nil {
		logrus.WithError(err).Error("find JSON textarea")
	}
	err = ta[0].Click()
	if err != nil {
		logrus.WithError(err).Error("click JSON textarea")
	}
	return nil
}

func (b *Bot) keysMetaV() error {
	err := b.wd.KeyDown(selenium.MetaKey)
	if err != nil {
		return errors.Wrap(err, "KeyDown meta")
	}
	err = b.wd.KeyDown("V")
	if err != nil {
		return errors.Wrap(err, "KeyDown v")
	}
	err = b.wd.KeyUp(selenium.MetaKey)
	if err != nil {
		return errors.Wrap(err, "KeyUp meta")
	}
	err = b.wd.KeyUp("V")
	if err != nil {
		return errors.Wrap(err, "KeyUp v")
	}

	//err := b.wd.KeyDown(selenium.MetaKey + "v")
	//if err != nil {
	//	return errors.Wrap(err, "KeyDown meta + v")
	//}
	//err = b.wd.KeyUp(selenium.MetaKey + selenium.InsertKey)
	//if err != nil {
	//	return errors.Wrap(err, "KeyUp meta + v")
	//}
	return nil
}

func (b *Bot) keysMetaABackspace() error {
	err := b.wd.KeyDown(selenium.MetaKey)
	if err != nil {
		return errors.Wrap(err, "KeyDown meta")
	}
	err = b.wd.KeyDown("a")
	if err != nil {
		return errors.Wrap(err, "KeyDown a")
	}
	err = b.wd.KeyUp(selenium.MetaKey)
	if err != nil {
		return errors.Wrap(err, "KeyUp meta")
	}
	err = b.wd.KeyUp("a")
	if err != nil {
		return errors.Wrap(err, "KeyUp a")
	}
	err = b.wd.KeyDown(selenium.BackspaceKey)
	if err != nil {
		return errors.Wrap(err, "KeyDown backspace")
	}
	err = b.wd.KeyUp(selenium.BackspaceKey)
	if err != nil {
		return errors.Wrap(err, "KeyUp backspace")
	}
	return nil
}

func (b *Bot) findJSONTab() (selenium.WebElement, error) {
	wes, err := b.wd.FindElements(selenium.ByTagName, "a")
	if err != nil {
		return nil, errors.Wrap(err, "find a tags")
	}
	for _, we := range wes {
		txt, _ := we.Text()
		if txt == "JSON view" {
			return we, nil
		}
	}
	return nil, errors.New("could not find JSON view tab")
}

// Bot enables communication with TFL website using Bot robot (screen scraping)
type Bot struct {
	screenshots bool
	selService  *selenium.Service
	wd          selenium.WebDriver
	masterWindowHandle string
}

func NewBot(screenshots, debug, headless bool) (Bot, error) {
	result := Bot{
		screenshots: screenshots,
	}

	selenium.SetDebug(debug)
	svc, err := selenium.NewChromeDriverService(chromedriverPath, driverPort)
	if err != nil {
		return Bot{}, errors.Wrap(err, "NewChromeDriverService")
	}
	result.selService = svc

	extBytes, err := ioutil.ReadFile("Vue.js-devtools-v5.1.1.crx")
	if err != nil {
		logrus.WithError(err).Fatal("read chrome extension file")
	}
	encodedData := make([]byte, base64.StdEncoding.EncodedLen(len(extBytes)))
	base64.StdEncoding.Encode(encodedData, extBytes)

	caps := selenium.Capabilities{"browserName": "chrome"}
	chromeCaps := chrome.Capabilities{
		Path: "",
		Args: []string{},
		Extensions: []string{string(encodedData)},
	}

	if headless {
		chromeCaps.Args = append(chromeCaps.Args, "--headless")
	}

	caps.AddChrome(chromeCaps)
	wd, err := selenium.NewRemote(caps, fmt.Sprintf("http://localhost:%d/wd/hub", driverPort))
	if err != nil {
		return Bot{}, errors.Wrap(err, "NewChromeDriverService")
	}
	result.wd = wd

	err = wd.SetPageLoadTimeout(pageLoadTimeout)
	if err != nil {
		return Bot{}, errors.Wrap(err, "set webdriver pageload timeout")
	}

	wh, err := wd.CurrentWindowHandle()
	if err != nil {
		return Bot{}, errors.Wrap(err, "get current window handle")
	}
	result.masterWindowHandle = wh

	return result, nil
}

func (b *Bot) LaunchFCS() error {
	err := b.wd.Get(fcsURL)

	if err != nil {
		return errors.Wrap(err, "launch fcs")
	}

	return nil
}

func (b *Bot) detectPartialURL(partialURL string) (bool, error){
	url, err := b.wd.CurrentURL()
	if err != nil {
		return false, errors.Wrap(err, "wd.CurrentURL()")
	}

	isConfig := strings.Contains(url, partialURL)

	return isConfig, nil
}

func (b *Bot) Stop() {
	b.selService.Stop()
	b.wd.Quit()
}

func (b *Bot) screenshot(name string) {
	if !b.screenshots {
		return
	}
	buf, err := b.wd.Screenshot()
	if err != nil {
		logrus.WithError(err).Error("Take screenshot")
		return
	}
	//save the screenshot to disk
	filename := fmt.Sprintf("screenshots/%s.png", name)
	_, err = os.Stat("screenshots")
	if os.IsNotExist(err) {
		err = os.Mkdir("screenshots", os.ModePerm|os.ModeDir)
		if err != nil {
			logrus.WithError(err).Error("Create screenshot directory")
		}
	}
	err = ioutil.WriteFile(filename, buf, os.ModePerm)
	if err != nil {
		logrus.WithError(err).Error("Create screenshot directory")
		return
	}
}

func clipboardCopyOSX(data []byte) error {
	copyCmd := exec.Command("pbcopy")

	in, err := copyCmd.StdinPipe()
	if err != nil {
		return err
	}

	if err := copyCmd.Start(); err != nil {
		return err
	}
	if _, err := in.Write(data); err != nil {
		return err
	}
	if err := in.Close(); err != nil {
		return err
	}
	return copyCmd.Wait()
}

func (b *Bot) doConfigActions() error {
	weJSONTab, err := b.findJSONTab()
	if err != nil {
		return errors.Wrap(err, "find JSON tab")
	}

	err = weJSONTab.Click()
	if err != nil {
		return errors.Wrap(err, "click JSON tab")
	}

	err = b.findSelectJSONTextArea()
	if err != nil {
		return errors.Wrap(err, "find select JSON text area")
	}

	err = b.keysMetaABackspace()
	if err != nil {
		return errors.Wrap(err, "send keystrokes meta+a+backspace")
	}

	err = b.keysMetaV()
	if err != nil {
		return errors.Wrap(err, "send keystrokes meta+v")
	}

	ozoneCfg, err := ioutil.ReadFile("ozone-config.json")
	if err != nil {
		return errors.Wrap(err, "read ozone config")
	}

	err = clipboardCopyOSX(ozoneCfg)
	if err != nil {
		return errors.Wrap(err, "copy clipboard OSX")
	}

	return nil
}

func (b *Bot) pollPage(partialURL string) error {
	for {
		isPage, err := b.detectPartialURL(partialURL)
		if err != nil {
			return errors.Wrapf(err, "detect partial url: %s", partialURL)
		}

		if isPage{
			break
		}
	}
	return nil
}

func (b *Bot) doStartPageActions() error {
	weDiscoveryBtn, err := b.wd.FindElement(selenium.ByCSSSelector, "#ob-v3-1-ozone")
	if err != nil {
		return errors.Wrap(err, "find ozone discovery link button")
	}

	err = weDiscoveryBtn.Click()
	if err != nil {
		return errors.Wrap(err, "click ozone discovery link button")
	}
	return nil
}

func (b *Bot) doDiscoveryPageActions() error {
	err := b.findSelectJSONTextArea()
	if err != nil {
		return errors.Wrap(err, "find select JSON text area")
	}

	err = b.keysMetaABackspace()
	if err != nil {
		return errors.Wrap(err, "send keystrokes meta+a+backspace")
	}

	discoBytes, err := ioutil.ReadFile("ozone-discovery.json")
	if err != nil {
		return errors.Wrap(err, "read ozone-discovery file")
	}

	err = clipboardCopyOSX(discoBytes)
	if err != nil {
		return errors.Wrap(err, "copy disco bytes to clipboard")
	}

	err = b.keysMetaV()
	if err != nil {
		return errors.Wrap(err, "send keystrokes meta+v")
	}

	//script := `this.$vm.editor.setValue("foobar")`
	//_, err = b.wd.ExecuteScript(script, nil)
	//if err != nil {
	//	return errors.Wrap(err, "execute editor script")
	//}

	return nil
}

func (b *Bot) doOverviewRunActions() error {
	// Check that the test cases have been rendered
	err := b.pollDOMItemTextValue(selenium.ByTagName, "h6", "API Specification")
	if err != nil {
		return errors.Wrap(err, "poll dom item text value - h6 - API Specification")
	}

	// Test cases rendered, now find the "PSU Consent" links
	weTags, err := b.wd.FindElements(selenium.ByTagName, "a")
	if err != nil {
		return errors.Wrap(err, "find a tags")
	}
	var psuElements []selenium.WebElement
	for _, tag := range weTags {
		txt, err := tag.Text()
		if err != nil {
			return errors.Wrap(err, "get tag text")
		}
		if txt == "PSU Consent" {
			psuElements = append(psuElements, tag)
		}
	}
	for _, psuLink := range psuElements {
		err = b.performPSU(psuLink)
		if err != nil {
			return errors.Wrap(err, "perform PSU")
		}
	}

	weNextButton, err := b.wd.FindElement(selenium.ByID, "next")
	if err != nil {
		return errors.Wrap(err, "find next (run) button")
	}
	err = weNextButton.Click()
	if err != nil {
		return errors.Wrap(err, "click next (run) button")
	}

	return nil
}

func (b *Bot) performPSU(linkElem selenium.WebElement) error {
	err := linkElem.Click()
	if err != nil {
		return errors.Wrap(err, "click PSU consent link element")
	}

	windowHandles, err := b.wd.WindowHandles()
	if err != nil {
		return errors.Wrap(err, "get window handles")
	}

	for _, handle := range windowHandles {
		err = b.wd.SwitchWindow(handle)
		if err != nil {
			return errors.Wrap(err, "switch window")
		}
		cURL, err := b.wd.CurrentURL()
		if err != nil {
			return errors.Wrap(err, "get current URL")
		}
		if strings.Contains(cURL, "https://modelobankauth2018.o3bank.co.uk:4101/auth") {
			// perform Ozone consent
			err = b.performOzoneConsent()
			if err != nil {
				return errors.Wrap(err, "perform Ozone consent")
			}
			break
		}
	}
	err = b.wd.SwitchWindow(b.masterWindowHandle)
	if err != nil {
		return errors.Wrap(err, "switch window (to master)")
	}

	return nil
}

func (b *Bot) postMsg(msg string) error {
	script := fmt.Sprintf(`alert('%s')`, msg)
	resp, err := b.wd.ExecuteScript(script, nil)
	if err != nil {
		return errors.Wrap(err, "execute alert message script")
	}
	fmt.Println(resp)
	return nil
}

func (b *Bot) pollDOMItemTextValue(domSelector, domIdentifier, expValue string) error {
	for {
		elements, err := b.wd.FindElements(domSelector, domIdentifier)
		if err != nil {
			return errors.Wrapf(err, "find elements by(%s), value(%s)", domSelector, domIdentifier)
		}

		for _, e := range elements {
			txt, err := e.Text()
			if err != nil {
				return errors.Wrap(err, "get element text value")
			}
			if txt == expValue {
				return nil
			}
		}
	}
	return nil
}

func (b *Bot) performOzoneConsent() error {

	// Page 1

	// User login
		// #loginName
		// #password
		// tag="Button" (type="submit") (text="Login")

	weInputUsername, err := b.wd.FindElement(selenium.ByID, "loginName")
	if err != nil {
		return errors.Wrap(err, "find loginName text input")
	}
	weInputPassword, err := b.wd.FindElement(selenium.ByID, "password")
	if err != nil {
		return errors.Wrap(err, "find password text input")
	}
	weButtons, err := b.wd.FindElements(selenium.ByTagName, "button")
	if err != nil {
		return errors.Wrap(err, "find button tags")
	}

	weInputUsername.SendKeys("mits")
	weInputPassword.SendKeys("mits")
	for _, we := range weButtons {
		txt, err := we.Text()
		if err != nil {
			return errors.Wrap(err, "get button text value")
		}
		if txt == "Login" {
			err = we.Click()
			if err != nil {
				return errors.Wrap(err, "click login button")
			}
			break
		}
	}


	// Page 2

	// Review permissions / request
		// Accounts
	// URL suffix ="aac.select-accounts"
	// input type = "checkbox"
	// tag="Button" (type="submit") (text="Confirm")

	url, err := b.wd.CurrentURL()
	if err != nil {
		return errors.Wrap(err, "get current URL")
	}
	if strings.HasSuffix(url, "aac.select-accounts") {
		weInputs, err := b.wd.FindElements(selenium.ByTagName, "input")
		if err != nil {
			return errors.Wrap(err, "find inputs")
		}
		for _, we := range weInputs {
			typ, err := we.GetAttribute("type")
			if err != nil {
				return errors.Wrap(err, "get input types")
			}
			if typ == "checkbox" {
				err = we.Click()
				if err != nil {
					return errors.Wrap(err, "click checkbox")
				}
			}
		}
	}

	weButtons, err = b.wd.FindElements(selenium.ByTagName, "button")
	if err != nil {
		return errors.Wrap(err, "find buttons")
	}
	for _, we := range weButtons {
		txt, err := we.Text()
		if err != nil {
			return errors.Wrap(err, "get button text")
		}
		if txt == "Confirm" {
			b.wd.ExecuteScript("arguments[0].scrollIntoView(true);", []interface{}{we})
			time.Sleep(time.Millisecond * 500)
			err = we.Click()
			if err != nil {
				return errors.Wrap(err, "click confirm button")
			}
			break
		}
	}
		// Payments
	// URL Suffix = "sdp.select-payment-account"
	//

	// Page 3

	// Url suffix ="aac.authorise-account-access-consent"
	// Yes button (class="btn btn-primary") (role="button") (tag="a") (text="Yes")

	url, err = b.wd.CurrentURL()
	if err != nil {
		return errors.Wrap(err, "get current URL")
	}
	if strings.HasSuffix(url, "aac.authorise-account-access-consent") {
		weATags, err := b.wd.FindElements(selenium.ByTagName, "a")
		if err != nil {
			return errors.Wrap(err, "find a tags")
		}
		for _, we := range weATags {
			txt, err := we.Text()
			if err != nil {
				return errors.Wrap(err, "get a text")
			}
			if txt == "Yes" {
				b.wd.ExecuteScript("arguments[0].scrollIntoView(true);", []interface{}{we})
				time.Sleep(time.Millisecond * 500)

				err = we.Click()
				if err != nil {
					return errors.Wrap(err, "click yes anchor href")
				}
				break
			}
		}
	}

	// Page 4 - FCS Callback url
	// Close widow button (text="Close Window") (tag="button") (type="Button")
	weButtonTags, err := b.wd.FindElements(selenium.ByTagName, "button")
	if err != nil {
		return errors.Wrap(err, "find button tags")
	}
	for _, we := range weButtonTags {
		txt, err := we.Text()
		if err != nil {
			return errors.Wrap(err, "get button text")
		}
		if txt == "Close Window" {
			err = we.Click()
			if err != nil {
				return errors.Wrap(err, "click yes anchor href")
			}
			break
		}
	}

	return nil
}